var data={};
var f="http://static.gamemorefun.net/gamefun"; 
var p="http://static.gamemorefun.net/public";
var t="http://app.gamemorefun.com";
var game={
	'en':{
		'pocketworld':{
			alert:f+"/img/alert/k.png",
			title:"pocketworld",
			bg:f+"/img/bg/pocketworld-640-835.jpg",
			fb:f+"http://www.facebook.com/pocketworld.eng",
			android:'http://m.onelink.me/9f72720',
			ios:'http://m.onelink.me/6dc3b724',
			guide:'http://app.gamemorefun.com/guidemobilesoldiers',
			lunbo:[
				f+'/img/lunbo/pocketworld1.jpg',
				f+'/img/lunbo/pocketworld2.jpg',
				f+'/img/lunbo/pocketworld3.jpg',
				f+'/img/lunbo/pocketworld4.jpg',
				f+'/img/lunbo/pocketworld5.jpg'
			]
		},
		'mobilesoldiers':{
			alert:f+"/img/alert/k.png",
			title:"mobilesoldiers",
			bg:f+"/img/bg/mobilesoldiers-640-835.jpg",
			facebook:'https://www.facebook.com/MobileSoldiers/',
			android:p+'/apk/mobilesoldiers.apk',
			guide:t+'/guidemobilesoldiers',
			num:9,
			lunbo:[
				f+'/img/lunbo/mobilesoldiers1.jpg',
				f+'/img/lunbo/mobilesoldiers2.jpg',
				f+'/img/lunbo/mobilesoldiers3.jpg',
				f+'/img/lunbo/mobilesoldiers4.jpg',
				f+'/img/lunbo/mobilesoldiers5.jpg'
			]
		}
		
	},
	'tw':{
		'17pk':{
			title:"神奇寶貝",
			bg:f+"/img/bg/17pk-640-835.jpg",
			android:'/apk/17pk1.apk',
			ios:'https://itunes.apple.com/us/app/bao-bei-jin-huago-deng-lu/id1074714030?l=zh&ls=1&mt=8',
			guide:'',
			lunbo:[
				f+'/img/lunbo/pocketworld1.jpg',
				f+'/img/lunbo/pocketworld2.jpg',
				f+'/img/lunbo/pocketworld3.jpg',
				f+'/img/lunbo/pocketworld4.jpg',
				f+'/img/lunbo/pocketworld5.jpg'
			]
		},

		'17duel':{
			title:"遊戲王",
			send:'sendma17duel',
			delete:'deletema17duel',
			bg:f+"/img/bg/17duel-640-835.jpg",
			fb:'https://www.facebook.com/17Duel/',
			android:p+'/apk/1/tw/17duel1.apk',
			ios:'http://m.onelink.me/edcbf0b1',
			guide:'http://app.gamemorefun.com/guide',
			lunbo:[
				f+'/img/lunbo/17duel1.jpg',
				f+'/img/lunbo/17duel2.jpg',
				f+'/img/lunbo/17duel3.jpg',
				f+'/img/lunbo/17duel4.jpg',
				f+'/img/lunbo/17duel5.jpg'
			]
		}
	}

}
exports.getData=function(lan,gamename){
	var gameValue=game[lan][gamename];
	for(var key in gameValue){
		if(!gameValue.hasOwnProperty(key)) continue;
		data[key]=gameValue[key];
	}
	return data;
}