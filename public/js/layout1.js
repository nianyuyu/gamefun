$(document).ready(function(){
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: 2500,
        autoplayDisableOnInteraction: false
        });

    var socket = io();
        socket.on('sendma17duel', function (data) {
        $(".code-style").text(data);
        if(!data){
        $(".code-style").html("<span class='content-admin'>兌換碼用完，請聯繫管理員</span>");
        $(".code-style").attr("class","code-style2");
        }
        console.log(data);
        });
        $(".show-dialog").on("click",function(){
        alertStyle();
        addBox();
        cancel();
        })

        
        function alertStyle(){
        var pHeight = document.documentElement.scrollHeight;
        var pWidth = document.documentElement.scrollWidth;
        var shadow = document.createElement("div");
        shadow.style.height = pHeight + "px";
        shadow.style.width = pWidth + "px";
        shadow.id = "shadow-box";
        document.body.appendChild(shadow);
        }
        function addBox(){
        var pWidth = document.documentElement.scrollWidth;
        var detailBox = document.getElementById("detail-box");
        $('#detail-box').css("display", "block");
        var elementLeft=(pWidth - detailBox.offsetWidth) / 2;
        $("#detail-box").css("top","30%")
        $("#detail-box").css("left",elementLeft);
        }
        function cancel(){
        var shadow=document.getElementById("shadow-box");
        $("#shadow-box").on("click",function(){
        $('#detail-box').css("display","none");
        document.body.removeChild(shadow);
        socket.emit('deletema');
        })
        }
})